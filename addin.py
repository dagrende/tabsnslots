#Author-Dag Rende
#Description-Add-In helper object - create add-ins without all boiler-plate code

from collections import defaultdict

import adsk.core, adsk.fusion
import math
import traceback
import re
import os

# pip install pyyaml -t packages; mv packages/yaml .; rm -rf packages
from . import yaml

import time
from . import utils

class AddIn(object):
    COMMAND_ID = None

    def __init__(self, commandId, commandName):
        self.COMMAND_ID = commandId
        self.commandName = commandName
        self.description = ""
        self.resourcePath = 'Resources'
        self.attributes = object()
        self.defaults = {}
        self.addDialogFields = None
        self.parseDialogFields = None
        self.fieldValidator = None
        self.executor = None

        self.app = adsk.core.Application.get()
        self.ui = self.app.userInterface

        self.handlers = utils.HandlerHelper()
        self.appPath = os.path.dirname(os.path.abspath(__file__))

    def writeDefaults(self):
        try:
            with open(os.path.join(self.appPath, 'defaults.yml'), 'w') as file:
                documents = yaml.dump(self.defaults, file)
        except:
            pass
    
    def readDefaults(self): 
        if not os.path.isfile(os.path.join(self.appPath, 'defaults.yml')):
            return
        with open(os.path.join(self.appPath, 'defaults.yml'), 'r') as file:
            self.defaults.update(yaml.safe_load(file))


    def addButton(self):
        # clean up any crashed instances of the button if existing
        try:
            self.removeButton()
        except:
            pass

        # add add-in to UI
        buttonDogbone = self.ui.commandDefinitions.addButtonDefinition(
            self.COMMAND_ID, self.commandName, self.description, self.resourcePath)

        buttonDogbone.commandCreated.add(self.handlers.make_handler(adsk.core.CommandCreatedEventHandler,
                                                                    self.onCreate))

        createPanel = self.ui.allToolbarPanels.itemById('SolidCreatePanel')
        buttonControl = createPanel.controls.addCommand(buttonDogbone, self.COMMAND_ID)

        # Make the button available in the panel.
        buttonControl.isPromotedByDefault = True
        buttonControl.isPromoted = True

    def removeButton(self):
        cmdDef = self.ui.commandDefinitions.itemById(self.COMMAND_ID)
        if cmdDef:
            cmdDef.deleteMe()
        createPanel = self.ui.allToolbarPanels.itemById('SolidCreatePanel')
        cntrl = createPanel.controls.itemById(self.COMMAND_ID)
        if cntrl:
            cntrl.deleteMe()

    def onCreate(self, args):
        self.readDefaults()

        inputs = args.command.commandInputs
        args.command.setDialogInitialSize(425, 475)
        args.command.setDialogMinimumSize(425, 475)

        if self.addDialogFields:
            self.addDialogFields(self, inputs)

        # Add handlers to this command.
        args.command.execute.add(self.handlers.make_handler(adsk.core.CommandEventHandler, self.onExecute))
        args.command.validateInputs.add(
            self.handlers.make_handler(adsk.core.ValidateInputsEventHandler, self.onValidate))
        args.command.inputChanged.add(
            self.handlers.make_handler(adsk.core.InputChangedEventHandler, self.onInputChanged))

    def parseInputs(self, inputs):
        inputs = {inp.id: inp for inp in inputs}

        if self.parseDialogFields:
            self.parseDialogFields(self, inputs)

    def onValidate(self, args):
        if self.fieldValidator:
            self.fieldValidator(self, args)

    def onExecute(self, args):

        app = adsk.core.Application.get()
        start = time.time()
        doc = app.activeDocument  
        design = app.activeProduct
        timeLine = design.timeline
        timeLineGroups = timeLine.timelineGroups
        timelineCurrentIndex = timeLine.markerPosition
        
        self.parseInputs(args.firingEvent.sender.commandInputs)
        self.writeDefaults()
        if self.executor:
            self.executor(self)
        
        timelineEndIndex = timeLine.markerPosition
        if timelineEndIndex - timelineCurrentIndex > 1:
            exportTimelineGroup = timeLineGroups.add(timelineCurrentIndex, timelineEndIndex-1)# the minus 1 thing works, weird.
            exportTimelineGroup.name = self.commandName
        
    def onInputChanged(self, args):
        cmd = args.firingEvent.sender

    @property
    def design(self):
        return self.app.activeProduct

    @property
    def rootComp(self):
        return self.design.rootComponent

    def log(self, msg):
        try:
            with open(os.path.join(self.appPath, 'log.txt'), 'a+') as file:
                file.write(msg + "\n")
        except:
            pass

