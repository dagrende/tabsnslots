#Author-Dag Rende
#Description-Create tabs-and-slots fittings

# Select a face to get tabs, and a face to extrude the tabs to, that is part of the body to get the slots
# Thanks to Casey Rogers, Patrick Rainsberry, David Liu and Gary Singer - the creators of the Dogbone 
# add-in - for a clean and simple Fusion 360 add-in template.

from collections import defaultdict

import adsk.core, adsk.fusion
import traceback

from . import utils
from . import addin

class TabsNSlotsAttributes:
    def __init__(self):
        self.tabFaces = []
        self.slotFace = None
        self.marginWidth = 0
        self.tabWidthMin = 0
        self.tabGapWidth = 0
        self.slotFilletRadius = 0

def addDialogFields(addIn, inputs):
    selInput0 = inputs.addSelectionInput(
        'selectTabFaces', 'Tab faces',
        'Select the face to add tabs to')
    selInput0.addSelectionFilter('Faces')
    selInput0.setSelectionLimits(1,0)

    selInput1 = inputs.addSelectionInput(
        'selectSlotFace', 'Slot face',
        'Select the face to make slots in, for the tabs.')
    selInput1.addSelectionFilter('Faces')
    selInput1.setSelectionLimits(1,1)

    marginWidth = inputs.addValueInput(
        'marginWidth', 'Margin width', addIn.design.unitsManager.defaultLengthUnits,
        adsk.core.ValueInput.createByString(addIn.defaults['marginWidth']))
    marginWidth.tooltip = "Distance from edge to start of tab."

    tabWidthMin = inputs.addValueInput(
        'tabWidthMin', 'Minimum tab width', addIn.design.unitsManager.defaultLengthUnits,
        adsk.core.ValueInput.createByString(addIn.defaults['tabWidthMin']))
    tabWidthMin.tooltip = "Minimum tab width."

    tabGapWidth = inputs.addValueInput(
        'tabGapWidth', 'Tab gap width', addIn.design.unitsManager.defaultLengthUnits,
        adsk.core.ValueInput.createByString(addIn.defaults['tabGapWidth']))
    tabGapWidth.tooltip = "Width of gaps between tabs."

    slotFilletRadius = inputs.addValueInput(
        'slotFilletRadius', 'Slow fillet radius', addIn.design.unitsManager.defaultLengthUnits,
        adsk.core.ValueInput.createByString(addIn.defaults['slotFilletRadius']))
    slotFilletRadius.tooltip = "Radius of fillet at short side, where tab enters."

def parseDialogFields(addIn, inputs):
    addIn.attributes.tabFaces = []
    tabFacesInput = inputs['selectTabFaces']
    for i in range(tabFacesInput.selectionCount):
        entity = tabFacesInput.selection(i).entity
        if entity.objectType == adsk.fusion.BRepFace.classType():
            addIn.attributes.tabFaces.append(entity)
    
    addIn.attributes.slotFace = inputs['selectSlotFace'].selection(0).entity

    addIn.defaults['marginWidth'] = inputs['marginWidth'].expression
    addIn.attributes.marginWidth = inputs['marginWidth'].value
    addIn.defaults['tabWidthMin'] = inputs['tabWidthMin'].expression
    addIn.attributes.tabWidthMin = inputs['tabWidthMin'].value
    addIn.defaults['tabGapWidth'] = inputs['tabGapWidth'].expression
    addIn.attributes.tabGapWidth = inputs['tabGapWidth'].value
    addIn.defaults['slotFilletRadius'] = inputs['slotFilletRadius'].expression
    addIn.attributes.slotFilletRadius = inputs['slotFilletRadius'].value

def fieldValidator(addIn, args):
    cmd = args.firingEvent.sender

    for input in cmd.commandInputs:
        if input.id == 'selectTabFaces':
            addIn.log(f'fieldValidator selectTabFaces sel cnt={input.selectionCount}')
            if input.selectionCount < 1:
                args.areInputsValid = False
        elif input.id == 'selectSlotFace':
            addIn.log(f'fieldValidator selectSlotFace sel cnt={input.selectionCount}')
            if input.selectionCount < 1:
                args.areInputsValid = False

def executor(addIn):
    if not addIn.design:
        raise RuntimeError('No active Fusion design')

    # make tabs for each selected face        
    for face in addIn.attributes.tabFaces:
        faceComponent = face.body.parentComponent
        sketches = faceComponent.sketches
        planes = faceComponent.constructionPlanes
        extrudes = faceComponent.features.extrudeFeatures
        combines = faceComponent.features.combineFeatures
        fillets = faceComponent.features.filletFeatures

        # find the longest edge around the face: maxEdge
        maxLength = 0
        maxEdge = None
        for edge in face.edges:
            edgeLength = edge.length
            if maxLength < edgeLength:
                maxLength = edgeLength
                maxEdge = edge
        if not maxEdge:
            raise RuntimeError("could not find longest edge of selected face")

        # calculate tab positions as lines dividing tab/non tab
        evaluator = maxEdge.evaluator
        (returnValue, startParameter, endParameter) = evaluator.getParameterExtents()
        if not returnValue:
            raise RuntimeError("longest edge is unbounded and can't be divided into tab positions")

        (returnValue, maxEdgeStartPoint) = evaluator.getPointAtParameter(startParameter)
        (returnValue, length) = evaluator.getLengthAtParameter(startParameter, endParameter)

        # calculate evaluator parameter values for tab divisions along the length
        minLength = addIn.attributes.marginWidth * 2 + addIn.attributes.tabWidthMin
        if minLength > length:
            utils.messageBox('Too small face to fit tabs and margins. At least {0:.3f} mm is needed.'.format(minLength * 10.0))
            return

        tabSpace = length - 2 * addIn.attributes.marginWidth + addIn.attributes.tabGapWidth
        tabCount = int(tabSpace / (addIn.attributes.tabWidthMin + addIn.attributes.tabGapWidth))
        tabWidth = (tabSpace -addIn.attributes.tabGapWidth * tabCount) / tabCount

        dividerPositions = []
        currentPos = addIn.attributes.marginWidth
        dividerPositions.append(currentPos)
        for i in range(tabCount):
            currentPos += tabWidth
            dividerPositions.append(currentPos)
            currentPos += addIn.attributes.tabGapWidth
            dividerPositions.append(currentPos)
        del dividerPositions[-1]

        # add sketch
        sketch = sketches.add(face)
        sketch.name = "tabs"

        dividerLines = adsk.core.ObjectCollection.create()
        for dividerPosition in dividerPositions:
            dividerParamValue = startParameter + dividerPosition * (endParameter - startParameter) / length
            # create a plane on a part of maxEdge, that is perpendicular to it
            (retval, pt1) = evaluator.getPointAtParameter(dividerParamValue)
            (retval, normal) = evaluator.getFirstDerivative(dividerParamValue)
            dividingPlane = adsk.core.Plane.create(pt1, normal)

            # add a sketch line perpendicular to maxEdge, that divides the sketch face into two profiles
            # find edge that intersects the plane and where
            intersectionPoint = None
            for edge in face.edges:
                if edge != maxEdge:
                    (crosses, pt) = utils.edgePlaneCrossing(edge, dividingPlane)
                    if crosses:
                        intersectionPoint = pt
                        break

            if intersectionPoint:
                # add divider line to sketch
                sketchLines = sketch.sketchCurves.sketchLines
                sketchLine = sketchLines.addByTwoPoints(sketch.modelToSketchSpace(pt1), sketch.modelToSketchSpace(intersectionPoint))
                dividerLines.add(adsk.core.Line3D.create(pt1, intersectionPoint))

        # calculate extrusion the extent by finding the opposite face of the body in front of the tab

        # extrude the tab profiles
        profiles = adsk.core.ObjectCollection.create()
        # sort by position along maxEdge
        profileList = [profile for profile in sketch.profiles]
        posAlongMaxEdge = lambda profile: utils.scalarProduct(maxEdgeStartPoint, profile.boundingBox.minPoint)
        profileList.sort(key=posAlongMaxEdge)
        for i in range(tabCount):
            profiles.add(profileList[i * 2 + 1])

        # extrude tabs to slotFace - other side of opposite body
        addIn.attributes.slotFace.body.isVisible = False
        extrudeInput = extrudes.createInput(profiles, adsk.fusion.FeatureOperations.JoinFeatureOperation)
        isChained = True
        extent_toentity = adsk.fusion.ToEntityExtentDefinition.create(addIn.attributes.slotFace, isChained)
        extrudeInput.setOneSideExtent(extent_toentity, adsk.fusion.ExtentDirections.PositiveExtentDirection)
        ext = extrudes.add(extrudeInput)
        addIn.attributes.slotFace.body.isVisible = True

        # use the newly created tabs to cut slots in the opposite body
        toolBodies = adsk.core.ObjectCollection.create()
        toolBodies.add(face.body)
        combineInput = combines.createInput(addIn.attributes.slotFace.body, toolBodies)
        combineInput.operation = adsk.fusion.FeatureOperations.CutFeatureOperation
        combineInput.isKeepToolBodies = True
        combines.add(combineInput)

        # add fillets to fit tabs more 
        edgesToFillet = adsk.core.ObjectCollection.create()
        for dividerLine in dividerLines:
            # find new edge near dividerLine
            for newEdge in addIn.attributes.slotFace.body.edges:
                if newEdge.geometry.curveType == adsk.core.Curve3DTypes.Line3DCurveType:
                    if dividerLine.isColinearTo(newEdge.geometry):
                        edgesToFillet.add(newEdge)
        if edgesToFillet.count > 0:
            filletInput = fillets.createInput()      
            filletInput.addConstantRadiusEdgeSet(edgesToFillet, adsk.core.ValueInput.createByReal(addIn.attributes.slotFilletRadius), True)
            fillet = fillets.add(filletInput)

addIn = addin.AddIn("tabsNSlots", 'TabsNSlots')
addIn.description = 'Creates tabs from one body into another body in front of it. Suitable for gluing together perpendicular walls.'
addIn.attributes = TabsNSlotsAttributes()
addIn.defaults = {
    'marginWidth': "3 mm",
    'tabWidthMin': "5 mm",
    'tabGapWidth': "3 mm",
    'slotFilletRadius': "1 mm"}
addIn.addDialogFields = addDialogFields
addIn.parseDialogFields = parseDialogFields
addIn.executor = executor
addIn.fieldValidator = fieldValidator

def run(context):
    try:
        addIn.addButton()
    except:
        utils.messageBox(traceback.format_exc())


def stop(context):
    try:
        addIn.removeButton()
    except:
        utils.messageBox(traceback.format_exc())
